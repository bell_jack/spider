import requests
import pymysql
import urllib
import time
import json
print('start:'+time.ctime())
# import numpy as np
db_settings = {
	"host": "127.0.0.1",
	"port": 3306,
	"user": "root",
	"password": "jack",
	"db": "life",
	"charset": "utf8"
}
from bs4 import BeautifulSoup
conn = pymysql.connect(**db_settings)
cursor = conn.cursor()

taipei = ['中正', '大同', '中山', '松山', '大安', '萬華', '信義', '士林', '北投', '內湖', '南港', '文山']
for county in taipei:
	host = "www.ettoday.net"
	resource = "https://www.ettoday.net/news/tag/"+county+"/"
	headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
		"Accept-Encoding": "gzip, deflate, br", 
		"Accept-Language": "zh-TW,zh;q=0.9", 
		"Host": host,  #目標網站 
		"Sec-Fetch-Dest": "document", 
		"Sec-Fetch-Mode": "navigate", 
		"Sec-Fetch-Site": "none", 
		"Upgrade-Insecure-Requests": "1", 
		"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36" #使用者代理
	}
	response = requests.get(url=resource, headers=headers)
	soup = BeautifulSoup(response.text, "html.parser")

	lists = soup.find(class_="part_pictxt_3")
	rs = lists.find_all("div", class_="piece")
	for p in rs:
		img = p.find("img", {"itemprop":"image"})['content']
		h3 = p.select_one("h3")
		a = h3.select_one("a")
		href = a.get("href")
		href = urllib.parse.unquote(href)
		# detail_rs = requests.get(url=href, headers=headers)
		# d_sup = BeautifulSoup(detail_rs.text, "lxml")
		# content = d_sup.find("div", {"itemprop":"articleBody"})
		select_sql = "SELECT EXISTS(SELECT * FROM foods WHERE subject=%s AND cate=%s) as have";
		cursor.execute(select_sql, (h3.get_text(), county))
		rs_exists = cursor.fetchone()
		if rs_exists[0] == 1:
			continue
		sql = "INSERT INTO foods(type, city, subject, cate, img, href) VALUES(%s, %s, %s, %s, %s, %s)"	
		cursor.execute(sql, ('ettoday', '台北市', h3.get_text(), county, img, href))
		conn.commit()
	time.sleep(3)
countys = ['板橋', '中和', '永和', '新莊', '土城', '汐止', '鶯歌', '淡水', '五股', '林口', '深坑', '坪林', '石門', '萬里', '雙溪', '烏來', '三重', '新店', '蘆洲', '樹林', '三峽', '瑞芳', '泰山', '八里', '石碇', '三芝', '金山', '平溪', '貢寮']
for county in countys:
	host = "www.ettoday.net"
	resource = "https://www.ettoday.net/news/tag/"+county+"/"
	headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
		"Accept-Encoding": "gzip, deflate, br", 
		"Accept-Language": "zh-TW,zh;q=0.9", 
		"Host": host,  #目標網站 
		"Sec-Fetch-Dest": "document", 
		"Sec-Fetch-Mode": "navigate", 
		"Sec-Fetch-Site": "none", 
		"Upgrade-Insecure-Requests": "1", 
		"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36" #使用者代理
	}
	response = requests.get(url=resource, headers=headers)
	soup = BeautifulSoup(response.text, "html.parser")

	lists = soup.find(class_="part_pictxt_3")
	rs = lists.find_all("div", class_="piece")
	for p in rs:
		img = p.find("img", {"itemprop":"image"})['content']
		h3 = p.select_one("h3")
		a = h3.select_one("a")
		href = a.get("href")
		href = urllib.parse.unquote(href)
		# detail_rs = requests.get(url=href, headers=headers)
		# d_sup = BeautifulSoup(detail_rs.text, "lxml")
		# content = d_sup.find("div", {"itemprop":"articleBody"})
		select_sql = "SELECT EXISTS(SELECT * FROM foods WHERE subject=%s AND cate=%s) as have";
		cursor.execute(select_sql, (h3.get_text(), county))
		rs_exists = cursor.fetchone()
		if rs_exists[0] == 1:
			continue
		sql = "INSERT INTO foods(type, city, subject, cate, img, href) VALUES(%s, %s, %s, %s, %s, %s)"	
		cursor.execute(sql, ('ettoday', '新北市', h3.get_text(), county, img, href))
		conn.commit()
	time.sleep(3)
cursor.close()
conn.close()
print('end:'+time.ctime())